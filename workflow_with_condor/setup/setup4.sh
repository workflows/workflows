# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Script providing a functional build and runtime environment for project
# AnalysisBase when sourced from either BASH or ZSH.
#

# Check if we are in BASH. In that case determining the directory holding
# the script is relatively easy: 

echo "STARTING SETUP"

echo ${BASH_SOURCE[0]}

if [ "x${BASH_SOURCE[0]}" = "x" ]; then
    echo "Running zsh"
    # This trick should do the right thing under ZSH:
    thisdir=$(dirname `print -P %x`)
    if [ $? != 0 ]; then
        echo "ERROR: This script must be sourced from BASH or ZSH"
        return 1
    fi
    # We are in ZSH, so feel free to use a ZSH specific formalism:
    if [[ $options[shwordsplit] != on ]]; then
        # Instruct ZSH to behave like BASH does while iterating over
        # space separated words:
        setopt shwordsplit
        AnalysisBase_UNSETSHWORDSPLIT=1
    fi
else
    echo "Running bash"
    echo $(dirname ${BASH_SOURCE[0]})
    # The BASH solution is a bit more straight forward:
    thisdir=$(dirname ${BASH_SOURCE[0]})
fi

echo "FINISHED SETUP"
