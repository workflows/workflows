##
## Environment configuration file for the ATLAS user. Can be sourced to set up
## the analysis release installed in the image.
##
#
## Set up a nice prompt:
#export PS1='\[\033[01;35m\][bash]\[\033[01;31m\][\u AnalysisBase-$AnalysisBase_VERSION]\[\033[01;34m\]:\W >\[\033[00m\] ';
#
## Set up the compiler:
#source /opt/lcg/gcc/6.2.0binutils/x86_64-slc6/setup.sh
#echo "Configured GCC from: /opt/lcg/gcc/6.2.0binutils/x86_64-slc6"
#
## Configure Calibration via HTTP
#export PATHRESOLVER_ALLOWHTTPDOWNLOAD=1
#
## Set up the analysis release:
#source /usr/AnalysisBase/*/InstallArea/*/setup.sh
echo "Configured AnalysisBase from: $AnalysisBase_DIR"