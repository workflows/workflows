# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Script providing a functional build and runtime environment for project
# AnalysisBase when sourced from either BASH or ZSH.
#

# Check if we are in BASH. In that case determining the directory holding
# the script is relatively easy:
if [ "x${BASH_SOURCE[0]}" = "x" ]; then
    # This trick should do the right thing under ZSH:
    thisdir=$(dirname `print -P %x`)
    if [ $? != 0 ]; then
        echo "ERROR: This script must be sourced from BASH or ZSH"
        return 1
    fi
    # We are in ZSH, so feel free to use a ZSH specific formalism:
    if [[ $options[shwordsplit] != on ]]; then
        # Instruct ZSH to behave like BASH does while iterating over
        # space separated words:
        setopt shwordsplit
        AnalysisBase_UNSETSHWORDSPLIT=1
    fi
else
    # The BASH solution is a bit more straight forward:
    thisdir=$(dirname ${BASH_SOURCE[0]})
fi

# Function removing the duplicates from a given environment variable
remove_duplicates() {
    # Make it clear that the function's argument is the name of the
    # environment variable:
    envname=$1
    # Make copy of the environment variable:
    eval "temp=\$$envname"
    # Reset the value of the environment variable:
    eval "${envname}="
    # Loop over the elements of the copy:
    while [ -n "${temp}" ]; do
        # Take the first element of the remaining list:
        x=${temp%%:*}
        # Check that it's not an empty string:
        if [ "$x" != "" ]; then
            # If it's an existing directory, get its absolute path:
            if [ -d "$x" ] && [ -r "$x" ] && [ -x "$x" ]; then
                x=$(\cd "$x";\pwd)
            fi
            # Get the current value of the environment variable:
            eval "envval=\$$envname"
            # Decide whether to add it to the environment or not:
            case ${envval}: in
                *:"$x":*) ;;
                *) eval "${envname}=\"${envval}:$x\"";;
            esac
        fi
        # Remove the element from the list:
        temp=${temp#*:}
    done
    # Remove the leading ":" from the path:
    eval "envval=\$$envname"
    envval=`echo $envval | cut -c2-`
    eval "${envname}=\"${envval}\""
    # Export it:
    export ${envname}
    # And finally clean up:
    unset temp x envname envval
}

# Decide about the environment status variable to be used:
ENV_VAR=AnalysisBase_SET_UP
if [ "$*" = "extonly" ]; then
    ENV_VAR=AnalysisBase_EXTONLY_SET_UP
elif [ "$*" = "relonly" ]; then
    ENV_VAR=AnalysisBase_RELONLY_SET_UP
fi
# Check what its current value is:
eval "ENV_VAR_VALUE=\$$ENV_VAR"

